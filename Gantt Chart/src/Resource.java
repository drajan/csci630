
import java.util.LinkedList;



public class Resource {
	String name;
	String emailId;
	int id;
	int current_endDay;
	int starts_from;
	
	LinkedList<Resource> linkedList = new LinkedList<Resource>();
	
	Resource() {
		
	}
	
	Resource(String name, String emailId, int id, int current_endDay, int starts_from) {
		this.name = name;
		this.emailId = emailId;
		this.id = id;
		this.current_endDay = current_endDay;
		this.starts_from = starts_from;
	}
	
	void printDependency(LinkedList<Resource> l1) {
		for(int i = 0; i< l1.size(); i++) {
				System.out.println("resource name:" + l1.get(i).name + "resource emailId:" + 
						l1.get(i).emailId + "resource id" + l1.get(i).id);
		}
	}

}
