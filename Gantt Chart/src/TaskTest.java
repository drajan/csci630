import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

import org.junit.BeforeClass;
import org.junit.Test;


public class TaskTest {
	private static Task task;
	private String filename1 = "C:\\Users\\Di\\workspace\\Gantt Chart\\src\\testResource.txt";
	private String filename2 =  "C:\\Users\\Di\\workspace\\Gantt Chart\\src\\testTask.txt";
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		task = new Task();
	}

	
	@Test
	public void testGetInput() throws FileNotFoundException {
		LinkedList<Task> ll = new LinkedList<Task>();
		LinkedList<Dependency> dl = new LinkedList<Dependency>();
		task.getInput(ll,dl, filename1, filename2);
		int value = ll.size();
		assertEquals(6, value);
	}

	@Test
	public void testComputeCriticalPath() throws FileNotFoundException {
		LinkedList<Task> ll = new LinkedList<Task>();
		LinkedList<Dependency> dl = new LinkedList<Dependency>();
		task.getInput(ll,dl, filename1, filename2);
		task.computeCriticalPath(ll, dl);
		int max_duration = dl.get(2).totalDuration;
		assertEquals(50, max_duration);
	}

	

	@Test
	public void testGetEndDay() throws FileNotFoundException {
		LinkedList<Task> ll = new LinkedList<Task>();
		LinkedList<Dependency> dl = new LinkedList<Dependency>();
		task.getInput(ll, dl, filename1, filename2);		
		assertEquals(0,task.getEndDay(ll, ll.get(0).name));
	}

	@Test
	public void testAllocateResource() throws FileNotFoundException {
		LinkedList<Task> ll = new LinkedList<Task>();
		LinkedList<Dependency> dl = new LinkedList<Dependency>();
		task.getInput(ll,dl, filename1, filename2);
		task.allocateResource(ll, ll.get(0).id,ll.get(0).startDay, ll.get(0).duration);
		assertNotNull(ll.get(0).resource);
	}

	@Test
	public void testCheckMaxDuration() throws FileNotFoundException {
		LinkedList<Task> ll = new LinkedList<Task>();
		LinkedList<Dependency> dl = new LinkedList<Dependency>();
		task.getInput(ll, dl, filename1, filename2);
		task.computeCriticalPath(ll, dl);
		task.checkMaxDuration(dl);
		 
		int value = dl.get(0).totalDuration;
		int actual_index = 0; 
		 for(int i=0;i<dl.size()-1;i++)
		 {
		   if (dl.get(i+1).totalDuration > value)
		   {
		      value  = dl.get(i+1).totalDuration;
		      actual_index = i+1;
		     
		   }
		 }
		 int expected_index = 3;
		assertEquals(expected_index, actual_index);
	}

	@Test
	public void testFormStack() throws FileNotFoundException {
		
		LinkedList<Task> ll = new LinkedList<Task>();
		LinkedList<Dependency> dl = new LinkedList<Dependency>();
		task.getInput(ll,dl, filename1, filename2);
		Stack<Task> s = new Stack<Task>();
		s = task.formStack(s, 3, ll, dl,1);
		int size = s.size();
		assertEquals(3, size);
		while(!s.empty())
			s.pop();
	}

	@Test
	public void testCheckForDependency() throws FileNotFoundException {
		LinkedList<Task> ll = new LinkedList<Task>();
		LinkedList<Dependency> dl = new LinkedList<Dependency>();
		task.getInput(ll,dl, filename1, filename2);
		task.computeCriticalPath(ll, dl);
		Stack<Task> stasks = new Stack<Task>();
		int nodeIndex = task.checkMaxDuration(dl);
		if(ll.get(nodeIndex).isScheduled != 1)
		{
			stasks.push(ll.get(nodeIndex));
			stasks = task.formStack(stasks, nodeIndex,ll,dl,1);
			while(!stasks.empty())
			{
					task.checkForDependency(stasks.pop(),ll,dl);
			}
		}
		int expected = 1;
		assertEquals(expected, ll.get(nodeIndex).isScheduled);
	}

	
	@Test
	public void testAutoLevel() throws FileNotFoundException {
		LinkedList<Task> ll = new LinkedList<Task>();
		LinkedList<Dependency> dl = new LinkedList<Dependency>();
		task.getInput(ll,dl, filename1, filename2);
		task.computeCriticalPath(ll, dl);
		task.autoLevel(ll, dl);
		ArrayList<Integer> expected_al = new ArrayList<Integer>();
		expected_al.add(72);
		expected_al.add(20);
		expected_al.add(51);
		expected_al.add(92);
		expected_al.add(30);
		expected_al.add(61);
		ArrayList<Integer> actual_al = new ArrayList<Integer>();
		int check = 0,actual = 0;
		int expected = 0;
		for(int i = 0; i<ll.size(); i++)
		{
			check = ll.get(i).endDay;
			if(check <= 0)
			{
				actual = -1;
				break;
			}
		}
		assertEquals(expected, actual);		
	}
}
