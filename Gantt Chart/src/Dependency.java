

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Dependency {
	//LinkedList<ArrayList<String>> ll = new LinkedList<ArrayList<String>>();
	//LinkedList<Dependency> ll = new LinkedList<Dependency>();
	
	ArrayList<String> name;
	int totalDuration;
	int isTraveresed;
	
	Dependency() {
		this.name = null;
		this.totalDuration = 0;
		this.isTraveresed = 0;
	}
	
	Dependency (ArrayList<String> s, int totalDuration, int isTraversed) {
		this.name = s;
		this.totalDuration = totalDuration;
		this.isTraveresed = isTraversed;
	}
	
	void addDependency(LinkedList<Dependency> dl,List<String> s,int duration, int isTraversed){
		name = new ArrayList<String>();
		name.addAll(s);
		Dependency dependency = new Dependency(name, duration,isTraversed);
		//ll.add(name);
		dl.add(dependency);
		//ll.add(dependency);
	}
	
	void print(LinkedList<Dependency> l1) {
		for(int i = 0; i< l1.size(); i++) {
			System.out.println("");
			for(int j = 0; j< l1.get(i).name.size(); j++) {
				System.out.print(l1.get(i).name.get(j));
			}
			System.out.println("Duration:"+ l1.get(i).totalDuration);
		}
	}
}
