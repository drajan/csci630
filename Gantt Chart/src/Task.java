

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class Task {
	
	static Dependency d = new Dependency();
	static Resource r = new Resource();
	
	String name;
	int duration;
	int id;
	int startDay;
	int endDay;
	int count;
	int isScheduled;
	Resource resource;
	Task() {
		this.duration = 0;
		this.name = "";
		this.id = 0;
		this.startDay = 0;
		this.endDay = 0;
		this.isScheduled = 0;

	}
	
	
	void getInput(LinkedList<Task> ll, LinkedList<Dependency> dl, String fileName1, String fileName2) throws FileNotFoundException {
		List<String> temp;
		
		int count = 0;
		int id = 0;
		File file = new File(fileName1);
		Scanner inputReader = new Scanner(file);
        while(inputReader.hasNextLine()) {
        	String line = inputReader.nextLine();
        	String array[] = line.split(",");
        	String name1 = array[0];
        	String emailId = array[1];
        	int current_day = 0;
        	int starts_from = -1;
        	resource = new Resource(name1, emailId, id, current_day, starts_from);
        	r.linkedList.add(resource);
        	id++;
        }
        inputReader.close();
        String dependency = "";
        file = new File(fileName2);
        inputReader = new Scanner(file);
        while(inputReader.hasNextLine()) {
        	String line = inputReader.nextLine();
        	String[] array = line.split(",");
        	dependency = "";
        	Task t = new Task();
        	t.id = count;
        	t.name = array[0];
        	t.duration = Integer.parseInt(array[1]);
        	t.startDay = 0;
        	t.endDay = 0;
        	t.isScheduled = 0;
        	int i = 2;
        	if(array.length > 2){
        		while(i < array.length) {
        			dependency = dependency + array[i];
        			if(i+1 < array.length && array[i+1] != null) {
        				dependency = dependency + ",";
        			}
        			i++;
        		}
        		temp = new ArrayList<String>(Arrays.asList(dependency.split(",")));
        		d.addDependency(dl,temp, 0, 0);
        	}
        	else {
        		temp = new ArrayList<String>();
        		temp.add(null);
        		d.totalDuration = t.duration;
        		d.addDependency(dl,temp, d.totalDuration, 0);
        	}
        	ll.add(t);
        	count++;
        }
        inputReader.close();
	}   
        
	
	/*void computeCriticalPath(LinkedList<Task> taskList, LinkedList<Dependency> dependList) {
		
	}*/
	void computeCriticalPath(LinkedList<Task> taskList, LinkedList<Dependency> dependList) {
		Stack<Task> stack = new Stack<Task>();
		Stack<Task> tStack = new Stack<Task>();
		Task task = new Task(); 
		//ArrayList<Integer> al = new ArrayList<Integer>();
		int tDuration = 0;
		for(int i = 0 ; i < dependList.size(); i++) {
			if(dependList.get(i).totalDuration == 0) {
				stack.push(taskList.get(i));
				tStack = this.formStack(stack, i, taskList,dependList,0);
				tDuration = 0;
				while(!tStack.empty()) {
					task = tStack.pop();
					//System.out.println("Task:"+task.name);
					tDuration = tDuration + task.duration;
				}
		//al.add(i, tDuration);
				dependList.get(i).totalDuration = tDuration;
			}
		}
		//for(int j = 0 ; j < al.size(); j++)
		//	System.out.println("printing arraylist: " + al.get(j) );
		/*for(int j = 0; j < d.ll.size(); j++){
		if(al.get(j) != null)
		d.ll.get(j).totalDuration = al.get(j);
		}*/
	}
	
	void display(LinkedList<Task> ll) {
		int itr = 0;
		for(itr = 0; itr <= ll.getLast().id; itr++){
			System.out.println("Task:" + ll.get(itr).name + " Duration: " + ll.get(itr).duration
					+" Start Day: "+ ll.get(itr).startDay + " End Day: "+ll.get(itr).endDay + " Resource: " +
					ll.get(itr).resource.name);
		}
	}

	int getEndDay(LinkedList<Task> ll,String taskName){
		int eDay = 0;
		for(int itr = 0; itr <= ll.getLast().id ; itr++){
			if(taskName.equals(ll.get(itr).name)){
				eDay = ll.get(itr).endDay;
				break;
			}
		}
		return eDay;
	}
	
	
	
	void allocateResource(LinkedList<Task> ll,int taskId,int start_day, int duration){
		int sDay = 0;
		int return_sDay = -1;
		int return_id = -1;
		int diff = 0;
		int itr = 0;
		int check_diff = -1;
		int starts_from = 0;
		for(itr = 0; itr <= r.linkedList.getLast().id; itr++){
			starts_from = r.linkedList.get(itr).starts_from;
			if((start_day < starts_from || starts_from == -1) && 
					((start_day + duration) <= (starts_from)))
			{
				sDay = start_day;
				return_sDay = sDay ;
			}
			else
			{
				if(start_day >= r.linkedList.get(itr).current_endDay )
					sDay = start_day;
				else
					sDay = r.linkedList.get(itr).current_endDay + 1;
				diff =  r.linkedList.get(itr).current_endDay - sDay;
			}
			if((return_sDay >= sDay || return_sDay == -1)&&(check_diff >= Math.abs(diff) || check_diff == -1)){
				return_sDay = sDay;
				check_diff = Math.abs(diff);
				return_id = itr;
			}
		}
		starts_from = r.linkedList.get(return_id).starts_from;
		if(return_sDay < starts_from || starts_from == -1)
			starts_from = return_sDay;
		ll.get(taskId).resource = new Resource(r.linkedList.get(return_id).name,
								  r.linkedList.get(return_id).emailId,
								  r.linkedList.get(return_id).id,
								  return_sDay + duration,
								  starts_from);
		
		ll.get(taskId).startDay = return_sDay;
		ll.get(taskId).endDay = return_sDay + duration;
		ll.get(taskId).isScheduled = 1;
		//System.out.println("Resource starts from:" + r.linkedList.get(return_id).starts_from);
		//System.out.println("Scheduled:"+ll.get(taskId).name + " Start Day:" + start_day);
		r.linkedList.get(return_id).current_endDay = return_sDay + duration;
		r.linkedList.get(return_id).starts_from = starts_from;
	}
	
	/*void autoLevel1(LinkedList<Task> ll, LinkedList<Dependency> dl){
		ll.get(0).endDay = ll.get(0).startDay + ll.get(0).duration;
		r.linkedList.get(0).current_endDay = ll.get(0).endDay;
		//ll.get(0).resource = new Resource(r.linkedList.get(0).name,r.linkedList.get(0).emailId,
			//	r.linkedList.get(0).id,ll.get(0).endDay);
		
		int itr = 1;
		int temp_startDay1 = 0;

		for(itr = 1; itr <= ll.getLast().id; itr++){
			int dlsize = d.ll.get(itr).name.size();
			String taskName = dl.get(itr).name.get(0);
			if(dlsize==1 && taskName == null){
				allocateResource(ll,itr,0 ,ll.get(itr).duration);
			}
		}
		for(itr = 1; itr <= ll.getLast().id; itr++){
			temp_startDay1 = 0;
		    if(dl.size()>0){
				int dlsize = d.ll.get(itr).name.size();
				if(dlsize>=1 && dl.get(itr).name.get(0)!=null){
					int final_eDay = 0;
					for(int j=0;j<dlsize;j++){
						String taskName = dl.get(itr).name.get(j);
						if(taskName != null){
							int eDay = getEndDay(ll,taskName);
							if(final_eDay < eDay){
								final_eDay = eDay;
								if(final_eDay > ll.get(itr).startDay)
									temp_startDay1 = final_eDay + 1;
							}
						}
					}
					allocateResource(ll,itr,temp_startDay1 ,ll.get(itr).duration);
				}
			}
		}
	}
	*/
	int checkMaxDuration(LinkedList<Dependency> dl)
	{
		int set_index = -1;
		int max_duration = 0;
		for(int i=0; i<dl.size(); i++)
		{
			//System.out.println(dl.get(i).totalDuration);
			if(max_duration < dl.get(i).totalDuration && dl.get(i).isTraveresed == 0)
			{
				set_index = i;
				max_duration = dl.get(i).totalDuration;
			}
		}
		dl.get(set_index).isTraveresed = 1;
		return set_index;
		
	}
	
	Stack<Task> formStack(Stack<Task> s, int nodeIndex,LinkedList<Task> ll,LinkedList<Dependency> dl, int check)
	{
		int iCount = 0;
		while(iCount < dl.get(nodeIndex).name.size())
		{
			if(dl.get(nodeIndex).name.get(iCount)!=null)
			{
				String taskName = dl.get(nodeIndex).name.get(iCount);
				for(int i=0; i<=ll.getLast().id;i++)
				{
					if(taskName.equals(ll.get(i).name))
					{
						if(check == 0) {
							if(s.search(ll.get(i)) == -1)
								s.push(ll.get(i));
							s = formStack(s,i,ll,dl,0);
						}
						else if(check == 1) {
							s.push(ll.get(i));
							s = formStack(s,i,ll,dl,1);
						}
					}
				}
			}
			iCount++;
		}
	return s;
	}
	
	void checkForDependency(Task topTask, LinkedList<Task> ll,LinkedList<Dependency> dl)
	{
		int i = topTask.id;
		int temp_startDay1 = 0;
		if(topTask.isScheduled != 1)
		{
			if(dl.get(i).name.size()==1 && dl.get(i).name.get(0) == null)
				allocateResource(ll,i,0 ,ll.get(i).duration);
			else
			{
				int dlsize = dl.get(i).name.size();
				if(dlsize>=1 && dl.get(i).name.get(0)!=null){
					int final_eDay = 0;
					for(int j=0;j<dlsize;j++){
						String taskName = dl.get(i).name.get(j);
						if(taskName != null){
							int eDay = getEndDay(ll,taskName);
							if(final_eDay < eDay){
								final_eDay = eDay;
								if(final_eDay > ll.get(i).startDay)
									temp_startDay1 = final_eDay + 1;
							}
						}
					}
					allocateResource(ll,i,temp_startDay1 ,ll.get(i).duration);
				}
			}
		}
		
	}
	
	void autoLevel(LinkedList<Task> ll, LinkedList<Dependency> dl){
		Stack<Task> stasks = new Stack<Task>();
		for(int i=0; i<dl.size(); i++)
		{
			int nodeIndex = checkMaxDuration(dl);
			//System.out.println(nodeIndex);
			if(ll.get(nodeIndex).isScheduled != 1)
			{
				stasks.push(ll.get(nodeIndex));
				stasks = formStack(stasks, nodeIndex,ll,dl,1);
				while(!stasks.empty())
				{
						checkForDependency(stasks.pop(),ll,dl);
				}
			}
		}
	}

	
	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		if(args.length < 2) {
			System.out.println("Error, usage: java ClasName inputfile");
			System.exit(1);
		}
		String fileName1 = args[0];
		String fileName2 = args[1];
		Task t = new Task();
		LinkedList<Task> ll = new LinkedList<Task>();
		LinkedList<Dependency> dl = new LinkedList<Dependency>();
		t.getInput(ll,dl,fileName1,fileName2);
		t.computeCriticalPath(ll, dl);
		t.autoLevel(ll, dl);
		t.display(ll);
	//	d.print(dl);
	}
}

