

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilterReader;
import java.io.IOException;
import java.util.LinkedList;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Combo;
//import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

public class GanttToolGUI {
	
	//LinkedList<Task> ll = new LinkedList<Task>();
	//LinkedList<Dependency> dl = new LinkedList<Dependency>();
	//LinkedList<Task> ll ;
	
	//LinkedList<Dependency> dl;
	//LinkedList<Resource> rl = new LinkedList<Resource>();
	LinkedList<Resource> rl;
	Task task;
	
	GanttToolGUI() {
		
	}
	

	protected Shell shell;
	private Text txtTaskName;
	private Text txtTaskDuration;
	
	String name, dependency;
	int duration;
	private Text txtResourceName;
	private Text txtResourceEmail;
	private Text txtTaskList;
	private Text txtDelTaskName;
	private Text txtDelTaskDur;
	private Text txtDelTaskDepend;
	private Text txtDelResourceName;
	GC gc;
	Display display;

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	public void drawchart() {
		gc = new GC(shell);
		gc.drawRectangle(368,0,1200,1000);
        gc.setBackground(display.getSystemColor(SWT.COLOR_GRAY)); 
        gc.fillRectangle(368,0,1200,1000);
		gc.setLineWidth(15);
		try 
		{
					int x1 , x2, y1, y2;
					y1 = 50;
					y2 = 50;
					
					int currentSize=task.ll.size();
					if(currentSize>=1)
					{
						for(int i=0;i<task.ll.size();i++)
						{
							int plotstartday=task.ll.get(i).startDay;
							//System.out.println("Printing start day: " + task.ll.get(i).startDay);
							int plotendday=task.ll.get(i).endDay;
							//System.out.println("Printing end day: " + task.ll.get(i).endDay);
							x1=plotstartday+450;
							x2=plotendday+450;
							
								//gc.drawText(task.ll.get(i).name + "-" + task.ll.get(i).resource.name, x1,y1-25 );
								gc.drawText(task.ll.get(i).name + "-" + task.ll.get(i).resource.name, x2+15,y1-15 );
								gc.drawLine(x1, y1, x2, y2);
								//System.out.println("printing ");
								/*if(task.d.dl.get(i).name.get(0) == null)
								{
									System.out.println("Checking the index:" + i);
									gc.drawLine((x1+x2)/2, y1, (x1+x2)/2, y2+10);
								}*/
															
								y1=y1+24;
								y2=y2+24;
						}
							
					}
					
						if(currentSize<1)
					{		
						MessageBox mb=new MessageBox(shell);
						mb.setMessage("Please add enough tasks and resources and hit Run on Menu options");
						mb.open();
					}
		} catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Create contents of the window.
	 */
	protected void createContents() 
	{
		//ll = new LinkedList<Task>();
		//dl = new LinkedList<Dependency>();
		//rl = new LinkedList<Resource>();
		task = new Task();
		
		
		shell = new Shell();
		shell.setSize(1200,1000);
		shell.setText("SWT Application");
		final Composite lhsCompositeMain = new Composite(shell, SWT.NONE);
		lhsCompositeMain.setBounds(0, 0, 368, 728);
		lhsCompositeMain.setVisible(false);
		
		
		final Label lblSelectATask = new Label(lhsCompositeMain, SWT.NONE);
		lblSelectATask.setBounds(10, 14, 101, 20);
		lblSelectATask.setText("Select A Task");
		
		
		final Label lblTaskName = new Label(lhsCompositeMain, SWT.NONE);
		lblTaskName.setBounds(10, 56, 100, 20);
		
		
		txtTaskName = new Text(lhsCompositeMain, SWT.BORDER);
		txtTaskName.setBounds(157, 56, 160, 21);
		//name = txtTaskName.getText();
		
		final Label lblTaskDuration = new Label(lhsCompositeMain, SWT.NONE);
		lblTaskDuration.setText("Task Duration");
		lblTaskDuration.setBounds(10, 100, 100, 20);
		
		txtTaskDuration = new Text(lhsCompositeMain, SWT.BORDER);
		txtTaskDuration.setBounds(157, 94, 160, 21);
		String time = txtTaskDuration.getText();
		//duration = Integer.parseInt(txtTaskDuration.getText());
		
		final Label lblTaskDependency = new Label(lhsCompositeMain, SWT.NONE);
		lblTaskDependency.setText("Task Dependency");
		lblTaskDependency.setBounds(10, 143, 126, 20);
		//dependency = txtTaskDependency.getText();
		
		txtResourceName = new Text(lhsCompositeMain, SWT.BORDER);
		txtResourceName.setBounds(157, 56, 147, 21);
		
		txtResourceEmail = new Text(lhsCompositeMain, SWT.BORDER);
		txtResourceEmail.setBounds(158, 94, 169, 21);
		
		final Label lblNoDependency = new Label(lhsCompositeMain, SWT.NONE);
		lblNoDependency.setBounds(159, 137, 170, 31);
		lblNoDependency.setText("No Dependencies to Add");
		
		final Composite deleteComposite = new Composite(shell, SWT.NONE);
		deleteComposite.setBounds(0, 0, 368, 728);
		deleteComposite.setVisible(false);
		
		Label lblSelectDelTask = new Label(deleteComposite, SWT.NONE);
		lblSelectDelTask.setLocation(20, 20);
		lblSelectDelTask.setSize(80, 20);
		lblSelectDelTask.setText("Select Task");
		Label lblTaskDelName = new Label(deleteComposite, SWT.NONE);
		lblTaskDelName.setBounds(20, 60, 80, 20);
		lblTaskDelName.setText("Task Name");
		Label lblTaskDelDur = new Label(deleteComposite, SWT.NONE);
		lblTaskDelDur.setBounds(20, 100, 100, 20);
		lblTaskDelDur.setText("Task Duration");
		Label lblTaskDelDepend = new Label(deleteComposite, SWT.NONE);
		lblTaskDelDepend.setBounds(20, 140, 120, 20);
		lblTaskDelDepend.setText("Task Dependency");	
		
		txtDelTaskName = new Text(deleteComposite, SWT.BORDER);
		txtDelTaskName.setEditable(false);
		txtDelTaskName.setBounds(200, 60, 80, 20);
		txtDelTaskDur = new Text(deleteComposite, SWT.BORDER);
		txtDelTaskDur.setEditable(false);
		txtDelTaskDur.setBounds(200, 100, 80, 20);
		txtDelTaskDepend = new Text(deleteComposite, SWT.BORDER);
		txtDelTaskDepend.setEditable(false);
		txtDelTaskDepend.setBounds(200, 140, 80, 20);
		
		final Composite delResourcecomposite = new Composite(shell, SWT.NONE);
		delResourcecomposite.setBounds(0, 0, 368, 728);
		delResourcecomposite.setVisible(false);
		
		final Combo delResourcecombo = new Combo(delResourcecomposite, SWT.NONE);
		delResourcecombo.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent e) {
			
			int index = delResourcecombo.getSelectionIndex();
			txtDelResourceName.setText(task.r.rl.get(index).name);
			txtDelResourceName.setEnabled(false);
		}
		});
		delResourcecombo.setBounds(150, 40, 97, 28);
		
		final Composite RunComposite = new Composite(shell, SWT.NONE);
		RunComposite.setBounds(0, 0, 368, 728);
		
		final Composite saveComposite = new Composite(shell, SWT.NONE);
		saveComposite.setBounds(0, 0, 368, 728);
		
		
		final Label lblNoDataToSave = new Label(saveComposite, SWT.NONE);
		lblNoDataToSave.setBounds(50, 50, 150, 20);
		
		final List listEditDependency = new List(lhsCompositeMain, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		listEditDependency.setBounds(157, 137, 71, 68);
		
		final List listDependency = new List(lhsCompositeMain, SWT.BORDER | SWT.MULTI |SWT.V_SCROLL);
		listDependency.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent e) {
			String Dependency = "";
			int i=0;
			int indices[] = listDependency.getSelectionIndices();
			int count = indices.length;
			while(i<count)
			{
				int index = indices[i];
				String indexItem = listDependency.getItem(index).trim();
				Dependency = Dependency + indexItem + ",";
				i++;
			}
			Dependency =Dependency.substring(0, Dependency.length()-1);
			//txtTaskDependency.setText(Dependency);
			//System.out.println(Dependency);
		}
		});
		listDependency.setBounds(157, 137, 71, 68);
		
		final Button btnAddMoreResource = new Button(lhsCompositeMain, SWT.NONE);
		btnAddMoreResource.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				txtResourceName.setText("");
				txtResourceEmail.setText("");
				listDependency.setVisible(false);
			}
		});
		
		btnAddMoreResource.setBounds(275, 287, 52, 25);
		btnAddMoreResource.setText("+");
		
		final Button btnSaveEdit = new Button(lhsCompositeMain, SWT.NONE);
		btnSaveEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				duration = Integer.parseInt(txtTaskDuration.getText().trim());
				name = txtTaskName.getText().trim();
				
				int indices[] = listEditDependency.getSelectionIndices();
				int iCount = indices.length;
				int i =0;
				String Dependency = "";
				
				if(iCount > 0)
				{
					while(i<iCount)
					{
						int index = indices[i];
						String indexItem = listDependency.getItem(index).trim();
						Dependency = Dependency + indexItem + ",";
						i++;
					}
					Dependency =Dependency.substring(0, Dependency.length()-1);
				}
				//txtTaskDependency.setText(Dependency);
				
				//dependency = txtTaskDependency.getText();
				dependency = Dependency;
				task.edit(name, duration, dependency, task.ll, task.d.dl);
			
				}
			});
		btnSaveEdit.setBounds(242, 218, 75, 25);
		btnSaveEdit.setText("Save");
		
		final Combo listEditTaskList = new Combo(lhsCompositeMain, SWT.BORDER | SWT.ARROW_DOWN );
		
		//listEditTaskList.
		listEditTaskList.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				listEditDependency.removeAll();
				lblNoDependency.setVisible(false);
				int index = listEditTaskList.getSelectionIndex();
				//if(index!=0)
				//{
					//index--;
					//System.out.println(task.ll.get(index).name);
					txtTaskName.setText(task.ll.get(index).name);
					txtTaskDuration.setText(String.valueOf(task.ll.get(index).duration));
					//System.out.println(task.d.dl.get(index).name.get(0));
					String dependencyList="";
					String delim = ",";
					int i = 0;
					/*while(i<task.d.dl.get(index).name.size())
					{
						if(task.d.dl.get(index).name.get(i) != null)
						{
							dependencyList = dependencyList + task.d.dl.get(index).name.get(i).toString();
							dependencyList = dependencyList + ",";
						
						}
						else
							dependencyList = dependencyList + ",";
						i++;
					}*/
				    for(int x=0; x < task.ll.size(); x++)
					{
						listEditDependency.add(task.ll.get(x).name);
					}
					int listEditDepenLength = listEditDependency.getItems().length;
					
					String listItem = "";
					String dependencyName = "";
					//dependencyName = task.d.dl.get(index).name.size();
					//System.out.println(task.d.dl.get(index).name.get(0));
					if(!task.d.dl.get(index).name.contains(null))
					//if(task.d.dl.get(index).name.get(0) != null)
					{
						for(int j=0; j< task.d.dl.get(index).name.size(); j++)
						{
							dependencyName = task.d.dl.get(index).name.get(j).trim();
							
								for(int k = 0; k<listEditDepenLength; k++)
								{
									listItem = listEditDependency.getItem(k).trim();
									System.out.println(dependencyName + " " + listItem);
									if(listItem.equals(dependencyName))
									{
										System.out.println("List:"+listEditDependency.getItem(k).toString() + " Dependen:" +task.d.dl.get(index).name.get(j).trim());
										listEditDependency.select(k);
										break;
									}
								}
						}
					}
					//System.out.println(dependencyList);
					//txtTaskDependency.setText(dependencyList.substring(0, dependencyList.length()-1));
				//}
			}
		});
		listEditTaskList.setBounds(157, 10, 160, 25);
		listEditTaskList.setText("Select Task");
		
		final Button btnResetResource = new Button(lhsCompositeMain, SWT.NONE);
		btnResetResource.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				txtResourceName.setText("");
				txtResourceEmail.setText("");
			}
		});
		btnResetResource.setBounds(36, 218, 75, 25);
		btnResetResource.setText("Reset");
		
		
		
		final Button btnReset = new Button(lhsCompositeMain, SWT.NONE);
		btnReset.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				txtTaskName.setText("");
				txtTaskDuration.setText("");
				//txtTaskDependency.setText("");
			}
		});
		btnReset.setBounds(35, 218, 75, 25);
		btnReset.setText("Reset");
		
		final Button btnSave = new Button(lhsCompositeMain, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{	
				duration = Integer.parseInt(txtTaskDuration.getText().trim());
				name = txtTaskName.getText().trim();
				int indices[] = listDependency.getSelectionIndices();
				int iCount = indices.length;
				int i =0;
				String Dependency = "";
				
				if(iCount > 0)
				{
					while(i<iCount)
					{
						int index = indices[i];
						String indexItem = listDependency.getItem(index).trim();
						Dependency = Dependency + indexItem + ",";
						i++;
					}
					Dependency =Dependency.substring(0, Dependency.length()-1);
				}
				dependency = Dependency;
				MessageBox ms=new MessageBox(shell);
				
				//if(Task name do not conflict and t gets saved then)
				ms.setMessage("Saved Successfully");
				task.getInput(task.ll, task.d.dl, name, duration, dependency);

			    listDependency.add(name);
				
			}
		});
		btnSave.setBounds(242, 218, 75, 25);
		btnSave.setText("Save");
		
		final Button btnAddMoreTask = new Button(lhsCompositeMain, SWT.NONE);
		btnAddMoreTask.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				lhsCompositeMain.setVisible(true);
				RunComposite.setVisible(false);
				listDependency.setVisible(true);
				listDependency.setSelection(-1);
				lblNoDependency.setVisible(false);
				listEditDependency.setVisible(false);
				txtTaskName.setText("");
				txtTaskDuration.setText("");
				//txtTaskDependency.setText("");
				/*listDependency.removeAll();
				int i=0;
				if(task.ll.size()!=0)
				{
					while(i<=task.ll.getLast().id)
					{
						listDependency.add(task.ll.get(i).name);
						i++;
					}
				}*/

				
			}
		});
		btnAddMoreTask.setBounds(278, 287, 39, 25);
		btnAddMoreTask.setText("+");

		final Combo delCombo = new Combo(deleteComposite, SWT.NONE);
		delCombo.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent e) {
			int index = delCombo.getSelectionIndex();
			txtDelTaskName.setText(task.ll.get(index).name);
			txtDelTaskDur.setText(String.valueOf(task.ll.get(index).duration));
			String dependencyList="";
		
			int i = 0;
			if(!task.d.dl.get(index).name.contains(null)) {
				while(i < task.d.dl.get(index).name.size())
				{
					dependencyList = dependencyList + task.d.dl.get(index).name.get(i).toString();
					dependencyList = dependencyList + ",";
					i++;
					System.out.println(i);
				}
			}
			
			if(dependencyList.length() > 0)
				txtDelTaskDepend.setText(dependencyList.substring(0, dependencyList.length()-1));
			}
		});
		delCombo.setBounds(200, 20, 100, 20);

		Button btnDelete = new Button(deleteComposite, SWT.NONE);
		btnDelete.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent e) {
			String name1 = txtDelTaskName.getText();
			task.delete(name1, task.ll, Task.d.dl);
			txtDelTaskName.setText("");
			txtDelTaskDur.setText("");
			txtDelTaskDepend.setText("");
			delCombo.remove(name1);
			//listEditTaskList.remove(name1);
			
			Task.d.print(Task.d.dl);
				if(task.ll.size() == 0) {
				task.ll.clear();
				task.d.dl.clear();
				}
			}
		});
		btnDelete.setBounds(200, 200, 80, 30);
		btnDelete.setText("Delete");
		
		final Label lblCannotRun = new Label(RunComposite, SWT.NONE);
		lblCannotRun.setBounds(50, 100, 200, 35);
		
		
		
		final Composite ViewResourceComposite = new Composite(shell, SWT.NONE);
		ViewResourceComposite.setBounds(0, 0, 368, 728);
		
		final Label lblResource = new Label(ViewResourceComposite, SWT.NONE);
		lblResource.setLocation(20, 40);
		lblResource.setSize(75, 20);
		lblResource.setText("Resource");
		
		final Combo comboResourceList = new Combo(ViewResourceComposite, SWT.NONE);
		
		comboResourceList.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String name = comboResourceList.getText();
				//System.out.println(task.ll.get(0).resource.name);
				String taskList = "";
				//System.out.println(task.ll.size());
				for(int i =0; i<task.ll.size(); i++)
				{
					if(task.ll.get(i).resource.name.equals(name))
						taskList = taskList + task.ll.get(i).name + ",";
					//System.out.println(task.ll.get(i).resource.name);
				}
				if(taskList != "")
					taskList = taskList.substring(0, taskList.length() - 1);
				else
					taskList = "No Tasks Allocated!";
				//System.out.println(taskList);
				txtTaskList.setText(taskList);
				
			}
		});
		comboResourceList.setLocation(120, 40);
		comboResourceList.setSize(91, 23);
		comboResourceList.setText("Select");
		
		final Label lblNoResourceAdded = new Label(ViewResourceComposite, SWT.NONE);
		lblNoResourceAdded.setBounds(32, 104, 170, 20);
		lblNoResourceAdded.setText("No Resources added yet");
		
		final Label lblTasks = new Label(ViewResourceComposite, SWT.NONE);
		lblTasks.setBounds(20, 80, 55, 20);
		lblTasks.setText("Tasks");
		
		txtTaskList = new Text(ViewResourceComposite, SWT.BORDER);
		txtTaskList.setBounds(120, 80, 154, 21);
		
		final Button btnSaveResource = new Button(lhsCompositeMain, SWT.NONE);
		btnSaveResource.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String name = txtResourceName.getText();
				String email = txtResourceEmail.getText();
				task.getResources(task.r.rl, name, email);
				//comboResourceList.add(name);
				
			}
		});
		btnSaveResource.setBounds(242, 218, 110, 25);
		btnSaveResource.setText("Save Resource");
		
		Button btnDelResource = new Button(delResourcecomposite, SWT.NONE);
		btnDelResource.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent e) {
			String name = txtDelResourceName.getText().trim();
			task.delResources(task.r.rl, name);
			delResourcecombo.remove(name);
			txtDelResourceName.setText("");
		}
		});
		btnDelResource.setBounds(150, 200, 90, 30);
		btnDelResource.setText("Delete");
		
		txtDelResourceName = new Text(delResourcecomposite, SWT.BORDER);
		txtDelResourceName.setBounds(150, 120, 78, 26);
		Label lblReourceName = new Label(delResourcecomposite, SWT.NONE);
		lblReourceName.setBounds(20, 120, 105, 20);
		lblReourceName.setText("Resource Name");
		Label lblSelectDelResource = new Label(delResourcecomposite, SWT.NONE);
		lblSelectDelResource.setBounds(20, 40, 105, 20);
		lblSelectDelResource.setText("Select Resource");
		
		Menu menuBar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menuBar);
		
		MenuItem menuBarProject = new MenuItem(menuBar, SWT.CASCADE);
		menuBarProject.setText("Project");
		
		
		Menu menu_1 = new Menu(menuBarProject);
		menuBarProject.setMenu(menu_1);
		
		MenuItem proOpenExistingProject = new MenuItem(menu_1, SWT.NONE);
		proOpenExistingProject.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				task = new Task();
				//LinkedList<Resource> rl = new LinkedList<Resource>();
				task.r.rl = new LinkedList<Resource>();
				task.d.dl = new LinkedList<Dependency>();
				listDependency.removeAll();
				listEditTaskList.removeAll();
				
				comboResourceList.removeAll();
				
				//System.out.println(task.ll.size());
				
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
		        fd.setFilterPath("C:/");
		        String[] filterExt = {"*.txt"};
		        fd.setFilterExtensions(filterExt);
		        String selected = fd.open();
		        //System.out.println(selected);
		        String fPath = fd.getFilterPath();
		        String fileNameRes = fd.getFileName().toString().substring(0, fd.getFileName().length()-4);
	        	//String filePath = "C:\\Users\\Di\\"+fileNameRes+"_Res.txt";
		        
		        String sfilePath = fPath+"\\"+fileNameRes+"_Res.txt";
		       
		        System.out.println(sfilePath);
		        
		        try {
					task.getInput_Open(task.ll, task.d.dl, task.r.rl,sfilePath,selected);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		       /* try {
					FileReader f0 = new FileReader(selected);
					BufferedReader br = new BufferedReader(f0);
					
					String line = null;
					while((line = br.readLine())!= null)
					{
						
					}
				} 
		        catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
		        
		       /* if(task.r.rl.size() > 0)
				{
					for(int i=0; i<task.r.rl.size(); i++ )
					{
						comboResourceList.add(task.r.rl.get(i).name);
					}
				}*/
		        if(task.ll.size() > 0)
				{
					for(int i=0; i< task.ll.size(); i++)
					{
						//listEditTaskList.add(task.ll.get(i).name);
						listDependency.add(task.ll.get(i).name);
					}
				}
			}
			
		});
		proOpenExistingProject.setText("Open Existing Project");
		
		MenuItem proSave = new MenuItem(menu_1, SWT.NONE);
		proSave.setText("&Save\tCTRL+S");
		proSave.setAccelerator(SWT.CTRL + 'S');
		proSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lhsCompositeMain.setVisible(false);
				RunComposite.setVisible(false);
				ViewResourceComposite.setVisible(false);
				saveComposite.setVisible(true);
				
				if(task.ll.size() == 0)
				{
					lblNoDataToSave.setText("No Tasks added Yet!");
				}
				else
				{
					FileDialog fd = new FileDialog(shell, SWT.SAVE);
			        fd.setFilterPath("C:/");
			        String[] filterExt = {"*.txt"};
			        fd.setFilterExtensions(filterExt);
			        String selected = fd.open();
			        String fPath = fd.getFilterPath();
			        //System.out.println(selected);
			        //FileWriter writer = null;
			        try {
			        FileWriter f0 = new FileWriter(selected);
			        	 //System.out.println(fd.getFileName().toString());
			        	 for(int i=0;i < task.ll.size(); i++){
			        		 	String dependencyWrite = "";
			        		 	for(int j=0; j< task.d.dl.get(i).name.size(); j++)
			        		 	{
			        		 		if(task.d.dl.get(i).name.get(j) == null)
			        		 			dependencyWrite = "";
			        		 		else
			        		 		{
			        		 			dependencyWrite = dependencyWrite + task.d.dl.get(i).name.get(j);
			        		 			dependencyWrite = dependencyWrite + ",";
			        		 		}
			        		 		if(dependencyWrite != "")
			        		 			dependencyWrite.substring(0, dependencyWrite.length()-1);
			        		 	}
					           // f0.append("\n");
			        		 	//String writeText = "";
			        		 	String resName = "";
			        		 	if (task.ll.get(i).resource == null)
			        		 		resName = "";
			        		 	else
			        		 		resName = task.ll.get(i).resource.name;
			        		 	//System.out.println("Resource:"+resName);
			        		 	String writeText = task.ll.get(i).id+","+task.ll.get(i).name+","+
			        		 						task.ll.get(i).duration+","+task.ll.get(i).startDay+","+
			        		 						task.ll.get(i).endDay+","+resName+","+dependencyWrite+"\r\n";
					         
					            f0.append(writeText);
					            writeText = "";
					        }
			        	 f0.close();
			        	 String fileNameRes = fd.getFileName().toString().substring(0, fd.getFileName().length()-4);
			        	 String filePath = fPath + "\\"+fileNameRes+"_Res.txt";
			        	 File file = new File(filePath);
			        	 
			        	 if(!file.exists())
			        	 {
			        		 file.setWritable(true,false);
			        		 file.createNewFile();
			        		 
			        	 }
			        	 //String path = "C:/Resources/fileRes.txt";
			        	 //FileWriter f1 = new FileWriter(path);
			        	 FileWriter f1 = new FileWriter(file.getAbsoluteFile());
			        	// String resourceData = rl.get(0).name+","+rl.get(0).emailId+"\r\n";
		        		 //System.out.println(resourceData);
			        	 if(task.r.rl.size() > 0)
			        	 {
				        	 for(int i=0; i < task.r.rl.size(); i++)
				        	 {
				        		 String resourceData = task.r.rl.get(i).name+","+task.r.rl.get(i).emailId+"\r\n";
				        		 f1.append(resourceData);
				        		 resourceData = "";
				        	 }
			        	 }
			        	 else
			        	 {
			        		 String resourceData ="-";
			        		 f1.append(resourceData);
			        	 }
			        	 f1.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		proSave.setText("Save");
		
		MenuItem proExit = new MenuItem(menu_1, SWT.NONE);
		proExit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shell.close();
			}
		});
		proExit.setText("Exit");
	
		MenuItem menuBarTask = new MenuItem(menuBar, SWT.CASCADE);
		menuBarTask.setText("Task");
		
		Menu menu = new Menu(menuBarTask);
		menuBarTask.setMenu(menu);
		
		MenuItem taskMenuAddTask = new MenuItem(menu, SWT.NONE);
		taskMenuAddTask.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				
				lhsCompositeMain.setVisible(true);
				delResourcecomposite.setVisible(false);
				RunComposite.setVisible(false);
				ViewResourceComposite.setVisible(false);
				saveComposite.setVisible(false);
				listEditTaskList.setVisible(false);
				listEditDependency.setVisible(false);
				lblSelectATask.setVisible(false);
				deleteComposite.setVisible(false);
				lblTaskName.setVisible(true);
				lblTaskName.setText("Task");
				lblTaskDuration.setVisible(true);
				lblTaskDuration.setText("Task Duration");
				lblTaskDependency.setVisible(true);
				lblTaskDependency.setText("Task Dependency");
				txtTaskName.setText(" ");
				txtTaskDuration.setText(" ");
			//	txtTaskDependency.setText(" ");
			//	txtTaskDependency.setVisible(false);
				listDependency.setVisible(true);
				txtTaskName.setVisible(true);
				txtTaskName.setEnabled(true);
				txtTaskDuration.setVisible(true);
				txtResourceName.setVisible(false);
				txtResourceEmail.setVisible(false);
				btnSaveResource.setVisible(false);
				btnResetResource.setVisible(false);
				btnSave.setVisible(true);
				btnReset.setVisible(true);
				btnAddMoreTask.setVisible(true);
				btnAddMoreResource.setVisible(false);
				btnSaveEdit.setVisible(false);
				listDependency.removeAll();
				int i=0;
				if(task.ll.size()!= 0)
				{
					listDependency.setVisible(true);
					lblNoDependency.setVisible(false);
					while(i<=task.ll.getLast().id)
					{
						listDependency.add(task.ll.get(i).name);
						i++;
					}
				}
				else
				{
					listDependency.setVisible(false);
					lblNoDependency.setVisible(true);
				}

			}
		});
		taskMenuAddTask.setText("Add Task");
		
		
		MenuItem taskEditExistingTask = new MenuItem(menu, SWT.NONE);
		taskEditExistingTask.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				listEditTaskList.removeAll();
				for(int i=0;i <task.ll.size(); i++)
				{
					listEditTaskList.add(task.ll.get(i).name.toString());
				}
				lblSelectATask.setVisible(true);
				listEditTaskList.setVisible(true);
				delResourcecomposite.setVisible(false);
				listEditDependency.setVisible(true);
				lhsCompositeMain.setVisible(true);
				RunComposite.setVisible(false);
				deleteComposite.setVisible(false);
				saveComposite.setVisible(false);
				ViewResourceComposite.setVisible(false);
				lblNoDependency.setVisible(false);
				lblTaskName.setText("Task");
				txtTaskName.setText(" ");
				txtTaskDuration.setText(" ");				
				//txtTaskDependency.setText(" ");
				lblTaskDuration.setVisible(true);
				lblTaskDuration.setText("Task Duration");
				lblTaskDependency.setVisible(true);
				lblTaskDependency.setText("Task Dependency");
				btnAddMoreTask.setVisible(false);
				//txtTaskDependency.setVisible(true);
				listDependency.setVisible(false);
				txtTaskName.setVisible(true);
				txtTaskName.setEnabled(false);
				txtTaskDuration.setVisible(true);
				btnSaveEdit.setVisible(true);
				txtResourceName.setVisible(false);
				txtResourceEmail.setVisible(false);
				btnSaveResource.setVisible(false);
				btnResetResource.setVisible(false);
				btnSave.setVisible(false);
				btnReset.setVisible(false);
				btnAddMoreResource.setVisible(false);
			}
			
		});
		taskEditExistingTask.setText("Edit Existing task");
		
		MenuItem taskDeleteTask = new MenuItem(menu, SWT.NONE);
		taskDeleteTask.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				deleteComposite.setVisible(true);
				lhsCompositeMain.setVisible(false);
				ViewResourceComposite.setVisible(false);
				
				delCombo.removeAll();
				if(task.ll.size() > 0)
				{
					for(int i =0; i < task.ll.size(); i++)
					{
						delCombo.add(task.ll.get(i).name);
					}
				}
			}
		});
		taskDeleteTask.setText("Delete Task");
		
		MenuItem menuBarResource = new MenuItem(menuBar, SWT.CASCADE);
		menuBarResource.setText("Resource");
		
		Menu menu_2 = new Menu(menuBarResource);
		menuBarResource.setMenu(menu_2);
		
		MenuItem resAddResource = new MenuItem(menu_2, SWT.NONE);
		resAddResource.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				lhsCompositeMain.setVisible(true);
				RunComposite.setVisible(false);
				delResourcecomposite.setVisible(false);
				deleteComposite.setVisible(false);
				ViewResourceComposite.setVisible(false);
				listEditTaskList.setVisible(false);
				saveComposite.setVisible(false);
				lblSelectATask.setVisible(false);
				lblTaskName.setText("ResourceName");
				listEditDependency.setVisible(false);
				lblTaskDuration.setText("Resouce Email");
				lblTaskDependency.setVisible(false);
				//txtTaskDependency.setVisible(false);
				listDependency.setVisible(false);
				txtTaskName.setVisible(false);
				txtTaskDuration.setVisible(false);
				txtResourceName.setVisible(true);
				txtResourceName.setText("");
				txtResourceEmail.setText("");
				txtResourceEmail.setVisible(true);
				btnAddMoreTask.setVisible(false);
				btnSave.setVisible(false);
				btnSaveResource.setVisible(true);
				btnSaveEdit.setVisible(false);
				btnResetResource.setVisible(true);
				btnAddMoreResource.setVisible(true);
				lblNoDependency.setVisible(false);
			}
		});
		resAddResource.setText("Add Resource");
		
		MenuItem delResource = new MenuItem(menu_2, SWT.NONE);
		delResource.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent e) {
			lhsCompositeMain.setVisible(false);
			RunComposite.setVisible(false);
			ViewResourceComposite.setVisible(false);
			deleteComposite.setVisible(false);
			saveComposite.setVisible(false);
			delResourcecomposite.setVisible(true);
			
			delResourcecombo.removeAll();
			for(int i=0; i< task.r.rl.size(); i++)
			{
				delResourcecombo.add(task.r.rl.get(i).name);
			}
		}
		});
		delResource.setText("Delete Resource");


		
		MenuItem menuBarView = new MenuItem(menuBar, SWT.CASCADE);
		menuBarView.setText("View");
		
		Menu menu_3 = new Menu(menuBarView);
		menuBarView.setMenu(menu_3);
	
		MenuItem menuViewResources = new MenuItem(menu_3, SWT.NONE);
		menuViewResources.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				//tblResourceAllocation
				lhsCompositeMain.setVisible(false);
				delResourcecomposite.setVisible(false);
				RunComposite.setVisible(false);
				ViewResourceComposite.setVisible(true);
				saveComposite.setVisible(false);
				if(task.r.rl.size() == 0)
				{
					lblNoResourceAdded.setVisible(true);
					lblResource.setVisible(false);
					comboResourceList.setVisible(false);
					lblTasks.setVisible(false);
					txtTaskList.setVisible(false);
				}
				else
				{
					lblNoResourceAdded.setVisible(false);
					lblResource.setVisible(true);
					comboResourceList.setVisible(true);
					lblTasks.setVisible(true);
					txtTaskList.setVisible(true);
					for(int i=0; i< task.r.rl.size(); i++)
					{
						comboResourceList.add(task.r.rl.get(i).name.toString());
					}
				}
			}
		});
		menuViewResources.setText("Resource Allocation");

		MenuItem menuBarRun = new MenuItem(menuBar, SWT.NONE);
		menuBarRun.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				task.reset_values(task.ll, task.d.dl, task.r.rl );
				comboResourceList.removeAll();
				delResourcecomposite.setVisible(false);
				lhsCompositeMain.setVisible(false);
				ViewResourceComposite.setVisible(false);
				RunComposite.setVisible(true);
				saveComposite.setVisible(false);
				if(task.r.rl.size() == 0)
				{
					lblCannotRun.setText("No Resources added yet!");
				}
				else if(task.ll.size() == 0)
					lblCannotRun.setText("No Tasks added yet");
				else
				{
					System.out.println(task.r.rl.size());
					RunComposite.setVisible(false);
					task.computeCriticalPath(task.ll, task.d.dl);
					task.autoLevel(task.ll, task.d.dl,task.r.rl);
					for(int i =0; i< task.ll.size(); i++)
					{
						System.out.println(task.ll.get(i).startDay + " "+ task.ll.get(i).endDay + " "+ task.ll.get(i).resource.name + "\n");
						
					}
				}
				drawchart();
			}
		});
		menuBarRun.setText("Run");
		
		
		/*if(task.ll.size()==0 || task.ll.get(0).endDay == 0)
			lblGanttToolMsg.setVisible(true);
		else
			lblGanttToolMsg.setVisible(false);*/

	}
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			GanttToolGUI window = new GanttToolGUI();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
