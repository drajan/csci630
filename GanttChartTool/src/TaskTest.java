import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

import org.junit.BeforeClass;
import org.junit.Test;


public class TaskTest {
	private static Task task;
	LinkedList <Dependency> dl;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		task = new Task();
	}

	
	@Test
	public void testGetInput() throws FileNotFoundException {
		LinkedList<Task> ll_1 = new LinkedList<Task>();
		LinkedList<Dependency> dl_1 = new LinkedList<Dependency>();
		task.getInput(ll_1, dl_1, "111", 30, "");
		task.getInput(ll_1 , dl_1, "211", 30, "111");
		int value = ll_1.size();
		assertEquals(2, value);
	}

	@Test
	public void testComputeCriticalPath() throws FileNotFoundException {
		LinkedList<Task> ll_2 = new LinkedList<Task>();
		LinkedList<Dependency> dl_2 = new LinkedList<Dependency>();
		task.getInput(ll_2, dl_2, "111", 30, "");
		task.getInput(ll_2 , dl_2, "211", 30, "111");
		task.getInput(ll_2 , dl_2, "311", 30, "211");
		task.computeCriticalPath(ll_2, dl_2);
		System.out.println(dl_2.get(1).name.get(0));
		int max_duration = dl_2.get(0).totalDuration;
		System.out.println(max_duration);
		assertEquals(30, max_duration);
	}

	

	@Test
	public void testGetEndDay() throws FileNotFoundException {
		LinkedList<Task> ll_3 = new LinkedList<Task>();
		LinkedList<Dependency> dl_3 = new LinkedList<Dependency>();
		task.getInput(ll_3, dl_3, "111", 30, "");
		task.getInput(ll_3 , dl_3, "211", 30, "111");	
		assertEquals(0,task.getEndDay(ll_3, ll_3.get(0).name));
	}

	/*@Test
	public void testAllocateResource() throws FileNotFoundException {
		LinkedList<Task> ll_4 = new LinkedList<Task>();
		LinkedList<Dependency> dl_4 = new LinkedList<Dependency>();
		LinkedList<Resource> rl_4 = new LinkedList<Resource>();
		task.getResources(rl_4, "R1", "r1@mail.com");
		task.getResources(rl_4, "R2", "r2@gmail.com");
		task.getInput(ll_4, dl_4, "111", 30, "");
		task.getInput(ll_4 , dl_4, "211", 30, "111");
		task.getInput(ll_4 , dl_4, "311", 30, "211");
		
		
		task.allocateResource(ll_4, ll_4.get(0).id,ll_4.get(0).startDay, ll_4.get(0).duration,rl_4);
		assertNotNull(ll_4.get(0).resource);
	}*/
	
	
	@Test
	public void testCheckMaxDuration() throws FileNotFoundException {
		LinkedList<Task> ll_5 = new LinkedList<Task>();
		LinkedList<Dependency> dl_5 = new LinkedList<Dependency>();
	//	task.getInput(ll, dl, filename1, filename2);
		
		task.getInput(ll_5, dl_5, "111", 30, "");
		task.getInput(ll_5 , dl_5, "211", 30, "111");
		task.getInput(ll_5 , dl_5, "311", 30, "211");
		
		task.computeCriticalPath(ll_5, dl_5);
		task.checkMaxDuration(dl_5);
		 
		int value = dl_5.get(0).totalDuration;
		int actual_index = 0; 
		 for(int i=0;i<dl_5.size()-1;i++)
		 {
		   if (dl_5.get(i+1).totalDuration > value)
		   {
		      value  = dl_5.get(i+1).totalDuration;
		      actual_index = i+1;
		     
		   }
		 }
		 int expected_index = 2;
		assertEquals(expected_index, actual_index);
	}

	@Test
	public void testFormStack() throws FileNotFoundException {
		
		LinkedList<Task> ll_6 = new LinkedList<Task>();
		LinkedList<Dependency> dl_6 = new LinkedList<Dependency>();
		//task.getInput(ll,dl, filename1, filename2);
		Stack<Task> s = new Stack<Task>();
		task.getInput(ll_6, dl_6, "111", 30, "");
		task.getInput(ll_6 , dl_6, "211", 30, "111");
		task.getInput(ll_6 , dl_6, "311", 30, "211");
		
		s = task.formStack(s, 2, ll_6, dl_6);
		int size = s.size();
		assertEquals(2, size);
		while(!s.empty())
			s.pop();
	}

	@Test
	public void testCheckForDependency() throws FileNotFoundException {
		LinkedList<Task> ll_7 = new LinkedList<Task>();
		LinkedList<Dependency> dl_7 = new LinkedList<Dependency>();
		LinkedList<Resource> rl_7 = new LinkedList<Resource>();
		task.getResources(rl_7, "R1", "r1@mail.com");
		task.getResources(rl_7, "R2", "r2@gmail.com");
		task.getInput(ll_7, dl_7, "111", 30, "");
		task.getInput(ll_7 , dl_7, "211", 30, "111");
		task.getInput(ll_7 , dl_7, "311", 30, "211");
		
		//task.getInput(ll,dl, filename1, filename2);
		task.computeCriticalPath(ll_7, dl_7);
		Stack<Task> stasks = new Stack<Task>();
		int nodeIndex = task.checkMaxDuration(dl_7);
		if(ll_7.get(nodeIndex).isScheduled != 1)
		{
			stasks.push(ll_7.get(nodeIndex));
		//	stasks = task.formStack(stasks, nodeIndex,ll,dl,1);
			while(!stasks.empty())
			{
					task.checkForDependency(stasks.pop(),ll_7,dl_7,rl_7);
			}
		}
		int expected = 1;
		assertEquals(expected, ll_7.get(nodeIndex).isScheduled);
	}

	
	@Test
	public void testAutoLevel() throws FileNotFoundException {
		LinkedList<Task> ll_new = new LinkedList<Task>();
		LinkedList<Dependency> dl_new = new LinkedList<Dependency>();
		LinkedList<Resource> rl_new = new LinkedList<Resource>();
		
		task.getInput(ll_new, dl_new, "111", 30, "");
		task.getInput(ll_new , dl_new, "211", 30, "111");
		//task.getInput(ll_new , dl_new, "311", 30, "");
		
		task.getResources(rl_new, "R1", "r1@mail.com");
		task.getResources(rl_new, "R2", "r2@mail.com");
		
		task.computeCriticalPath(ll_new, dl_new);
		System.out.println("Computed");
		task.autoLevel(ll_new, dl_new,rl_new);
		int check = 0,actual = 0;
		int expected = 0;
		for(int i = 0; i<ll_new.size(); i++)
		{
			check = ll_new.get(i).endDay;
			if(check <= 0)
			{
				actual = -1;
				break;
			}
		}
		assertEquals(expected, actual);		
	}
}
